#include <CayenneLPP.h>
#include <DHTesp.h>
#include <HardwareSerial.h>
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <Wire.h>
#include <math.h>
#include <SSD1306.h>

//GPS dependancies (for NEO-M8N, NEO-6M etc)
#include <minmea.h>
#include "driver/uart.h"
#include "minmea.h" //GPS decoding
#define GPS_TX_PIN (34) //Pin to receive GPS transmistions


// LoRaWAN NwkSKey, network session key
static const PROGMEM u1_t NWKSKEY[16] = { 0x1D, 0xB4, 0x86, 0xB3, 0x22, 0xBD, 0x0B, 0xD9, 0x08, 0x91, 0xF6, 0xF7, 0x94, 0x77, 0x04, 0x32 };
// LoRaWAN AppSKey, application session key
static const u1_t PROGMEM APPSKEY[16] = { 0x09, 0x8E, 0x48, 0x37, 0xFB, 0x8C, 0x31, 0x7F, 0x94, 0xBB, 0xB2, 0xA6, 0x28, 0xF7, 0x0F, 0xDA };
// LoRaWAN end-device address (DevAddr)
static const u4_t DEVADDR = { 0x26021EF5 };
// These callbacks are only used in over-the-air activation, so they are
// left empty here (we cannot leave them out completely unless
// DISABLE_JOIN is set in config.h, otherwise the linker will complain).
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }

static osjob_t sendjob;
// Schedule data trasmission in every this many seconds (might become longer due to duty
// cycle limitations).
// we set 10 seconds interval
const unsigned TX_INTERVAL = 30; // Fair Use policy of TTN requires update interval of at least several min. We set update interval here of 1 min for testing

// Pin mapping according to Cytron LoRa Shield RFM
const lmic_pinmap lmic_pins = {
  .nss = 18,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = 14,
  .dio = {26, 33, 32},
};

//GPS readline function
char *readLine(uart_port_t uart) {
  static char line[256];
  int size;
  char *ptr = line;
  while(1) {
    size = uart_read_bytes(uart, (unsigned char *)ptr, 1, portMAX_DELAY);
    if (size == 1) {
      if (*ptr == '\n') {
        ptr++;
        *ptr = 0;
        return line;
      }
      ptr++;
    } // End of read a character
  } // End of loop
} // End of readLine

//DHT22 temperature/humidity sensor
int dhtPin = 17;
DHTesp dht;

CayenneLPP lpp(15);
SSD1306  display(0x3c, 4, 15);

void onEvent (ev_t ev) 
{
  Serial.print(os_getTime());
  Serial.print(": ");
  switch(ev) 
  {
    case EV_TXCOMPLETE:
      Serial.printf("EV_TXCOMPLETE (includes waiting for RX windows)\r\n");
      // Schedule next transmission
      os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
      break;  
    case EV_RXCOMPLETE:
      if (LMIC.dataLen)
      {
        Serial.printf("Received %d bytes\n", LMIC.dataLen);
      }
      break;
    default:
      Serial.printf("Unknown event\r\n");
      break;
  }
}

void do_send(osjob_t* j)
{
  // Check if there is not a current TX/RX job running
  if (LMIC.opmode & OP_TXRXPEND) 
  {
    Serial.printf("OP_TXRXPEND, not sending\r\n");
  } 
  else
  if (!(LMIC.opmode & OP_TXRXPEND)) 
  {


    delay(dht.getMinimumSamplingPeriod());
    
    TempAndHumidity newValues = dht.getTempAndHumidity();
    
    lpp.reset();
    lpp.addTemperature(1, newValues.temperature);
    lpp.addRelativeHumidity(2, newValues.humidity);
    
    Serial.printf("Temperature : %.2f, Humidity : %.2f\r\n", newValues.temperature, newValues.humidity);

    display.clear();
    display.drawString(0, 0, "Temperature: ");
    display.drawString(90, 0, String(newValues.temperature));
    display.drawString(0, 20, "Humidity  : ");
    display.drawString(90,20, String(newValues.humidity));
    display.display();    

    // Prepare upstream data transmission at the next possible time.
    LMIC_setTxData2(1, lpp.getBuffer(), lpp.getSize(), 0);
         
    Serial.printf("Packet queued\r\n");
  }
  // Next TX is scheduled after TX_COMPLETE event.
}

void do_gps() {
  Serial.printf("do_gps init\r\n");
  uart_config_t myUartConfig;
  myUartConfig.baud_rate           = 9600;
  myUartConfig.data_bits           = UART_DATA_8_BITS;
  myUartConfig.parity              = UART_PARITY_DISABLE;
  myUartConfig.stop_bits           = UART_STOP_BITS_1;
  myUartConfig.flow_ctrl           = UART_HW_FLOWCTRL_DISABLE;
  myUartConfig.rx_flow_ctrl_thresh = 120;

  uart_param_config(UART_NUM_1, &myUartConfig);

  uart_set_pin(UART_NUM_1,
      UART_PIN_NO_CHANGE, // TX
      GPS_TX_PIN,         // RX
      UART_PIN_NO_CHANGE, // RTS
      UART_PIN_NO_CHANGE  // CTS
  );

  uart_driver_install(UART_NUM_1, 2048, 2048, 10, NULL, 0);

  while(1) {
    char *line = readLine(UART_NUM_1);
    switch(minmea_sentence_id(line)) {
    case MINMEA_SENTENCE_RMC:
      //Serial.printf("Sentence - MINMEA_SENTENCE_RMC\r\n");
      struct minmea_sentence_rmc frame;
      if (minmea_parse_rmc(&frame, line)) {
        if(isnan(minmea_tocoord(&frame.latitude))){
          Serial.printf("No GPS lock...waiting\r\n");
        }else{
          Serial.printf("$xxRMC floating point degree coordinates and speed: (%f,%f) %f \r\n",
                  minmea_tocoord(&frame.latitude),
                  minmea_tocoord(&frame.longitude),
                  minmea_tofloat(&frame.speed));         
        }
      }
      else {
        Serial.printf("$xxRMC sentence is not parsed\r\n");
      }
      break;
    case MINMEA_SENTENCE_GGA:
      //Serial.printf("Sentence - MINMEA_SENTENCE_GGA\r\n");
      break;
    case MINMEA_SENTENCE_GSV:
      //Serial.printf("Sentence - MINMEA_SENTENCE_GSV\r\n");
      break;
    default:
      //Serial.printf("Sentence - other\r\n");
      break;
    }
  }
} // do_gps

void setup() 
{
  Serial.begin(115200); //Console logging
  Serial.printf("Starting...\r\n");

  pinMode(16,OUTPUT);
  digitalWrite(16, LOW); // set GPIO16 low to reset OLED
  delay(50);
  digitalWrite(16, HIGH);
  display.init();
  display.setFont(ArialMT_Plain_10);
  
  dht.setup(dhtPin, DHTesp::DHT22);
  
  // LMIC init
  os_init();
  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();
  // Set static session parameters. Instead of dynamically establishing a session
  // by joining the network, precomputed session parameters are be provided.
  uint8_t appskey[sizeof(APPSKEY)];
  uint8_t nwkskey[sizeof(NWKSKEY)];
  memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
  memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
  LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
  // Select frequencies range
  //LMIC_selectSubBand(0);
  LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);
  // Disable link check validation
  LMIC_setLinkCheckMode(0);
  // TTN uses SF9 for its RX2 window.
  LMIC.dn2Dr = DR_SF9;
  // Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
  LMIC_setDrTxpow(DR_SF7,14);
  Serial.printf("LMIC setup done!\r\n");
  // Start job
  do_send(&sendjob);

  //Start GPS polling
  do_gps();
}

void loop() 
{
  // Make sure LMIC is ran too
  os_runloop_once();

}

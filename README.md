# ESP32SingleChannelGateway
A mod of a Fork of the popular ESP8266 single channel gateway adapted to ESP32.
https://github.com/vpcola/ESP32SingleChannelGateway

Changes:
 - 868Mhz band.
 - Generic GPS (i.e. NEO-M8N, NEO-6M etc)

# Instructions
Dependancies
- Copy the libraries to your ~/Arduino/libraries

Gateway:
- Change SSID and Password to suit

Node:
- Register https://console.thethingsnetwork.org/
- Create applicaton
- Create device
- Use ABP Activation method
- Change NWKSKEY, APPSKEY and DEVADDR in code to ones generated
